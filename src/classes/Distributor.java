package classes;

public class Distributor {
    static private int nrOfDistributors = 0;
    private String name;
    private int licenseNr;
    private String cardID;
    private float account;


    public Distributor(String name, String cardID, float account) {
        this.name = name;
        this.licenseNr = nrOfDistributors++;
        this.cardID = cardID;
        this.account = account;
        Thread thread = new Thread();
        thread.start();
    }

    public Distributor() {

    }

    public String getNickname() {
        return name;
    }
    public void setNickname(String nickname) {
        this.name = nickname;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public void setAccount(float account) {
        this.account = account;
    }

    public Distributor genRandDistributor(String name){
        Distributor distributor = new Distributor();
        distributor.setNickname(name);
        distributor.licenseNr = nrOfDistributors++;
        distributor.setCardID(new Functions().randCardID());
        distributor.setAccount(new Functions().randFloatBetween(50,100));
        return distributor;
    }
    @Override
    public String toString() {
        return "Distributor{" +
                "name='" + name + '\'' +
                ", licenseNr=" + licenseNr +
                ", cardID='" + cardID + '\'' +
                ", account=" + account +
                '}';
    }
}
