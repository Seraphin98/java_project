package classes;

public class Abonament {
    private String type;
    private int devices;
    private String resolution;
    private float price;

    public Abonament(String type){
        this.type = type;
        switch(this.type){
            case "Basic":
                this.devices = 1;
                this.resolution = "720p";
                this.price = (float) 4.99;
                break;
            case "Family":
                this.devices = 4;
                this.resolution = "1080p";
                this.price = (float) 9.99;
                break;
            case "Premium":
                this.devices = 6;
                this.resolution = "4K";
                this.price = (float) 19.99;
                break;
        }
    }

    public String getType() {
        return type;
    }


    @Override
    public String toString() {
        return "Abonament{" +
                "type='" + type + '\'' +
                ", devices=" + devices +
                ", resolution='" + resolution + '\'' +
                ", price=" + price +
                '}';
    }
}
