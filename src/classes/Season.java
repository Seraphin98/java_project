package classes;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Season {
    private String title;
    private int nrOfEpisodes;
    private ArrayList<Episode> episodes;
    private GregorianCalendar dateOfPremiere;
    private int duration;
    private float mark;
    private float price;

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public ArrayList<Episode> getEpisodes() {
        return episodes;
    }
    public void setEpisodes(ArrayList<Episode> episodes) {
        this.episodes = episodes;
    }
    public float getMark() {
        return mark;
    }
    public void setMark(float mark) {
        this.mark = mark;
    }
    public float getPrice() {
        return price;
    }
    public void setPrice(float price) {
        this.price = price;
    }
    public int getNrOfEpisodes() {
        return nrOfEpisodes;
    }
    public void setNrOfEpisodes(int nrOfEpisodes) {
        this.nrOfEpisodes = nrOfEpisodes;
    }
    public GregorianCalendar getDateOfPremiere() {
        return dateOfPremiere;
    }
    public void setDateOfPremiere(GregorianCalendar dateOfPremiere) {
        this.dateOfPremiere = dateOfPremiere;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Season(){}

    public Season genRandSeason(){
        Season season = new Season();

        ArrayList<Episode> tmpepisodes = new ArrayList<>();
        GregorianCalendar date = new Functions().randDateBetweenYear(1950,2000);
        season.setNrOfEpisodes(new Functions().randIntBetween(10,20));
        season.setDateOfPremiere(date);
        for(int i = 0;i<season.getNrOfEpisodes();i++){
            Episode episode = new Episode();
            episode.setEpisodeTitle("Episode " + (i+1) );
            episode.setLength(new Functions().randIntBetween(30,60));
            season.setDuration(season.getDuration() + episode.getLength());
            episode.setMark(new Functions().randFloatBetween((float)0.1,10));
            episode.setEpPremiere(date);
            tmpepisodes.add(episode);
        }
        season.setEpisodes(tmpepisodes);
        setMark(new Functions().randFloatBetween((float)0.1,(float)10.0));
        if(season.getMark()>(float)7.0){
            season.setPrice((float)2.99);
        }else{
            if(season.getMark()>=(float)3.0){
                season.setPrice((float)1.99);
            }else{
                season.setPrice((float)0.99);
            }
        }
        return season;
    }

    @Override
    public String toString() {
        return "Season{" +
                "title='" + title + '\'' +
                ", nrOfEpisodes=" + nrOfEpisodes +
                ", episodes=" + episodes +
                ", dateOfPremiere=" + new Functions().dateToString(dateOfPremiere) +
                ", duration=" + duration +
                ", mark=" + mark +
                ", price=" + price +
                '}';
    }
}
