package classes;

import java.util.ArrayList;

public class Film extends Media {
    private int nrOfActors;
    private ArrayList<Actor> actors;
    private String trailer;
    private int expirationTime;
    private Discount discount;

    public ArrayList<Actor> getActors() {
        return actors;
    }
    public void setActors(ArrayList<Actor> actors) {
        this.actors = actors;
    }
    public String getTrailer() {
        return trailer;
    }
    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public void setExpirationTime(int expirationTime) {
        this.expirationTime = expirationTime;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }
    public int getNrOfActors() {
        return nrOfActors;
    }
    public void setNrOfActors(int nrOfActors) {
        this.nrOfActors = nrOfActors;
    }

    public Film genRandFilms(){
        Film film = new Film();
        film.setTitle(new Functions().getRandTitle1() + new Functions().getRandTitle2());
        film.setAuthor(new Functions().getRandName() + " " + new Functions().getRandSurname());
        film.setGenre(new Functions().getRandGenre());
        film.setCountries(new Functions().getRandCountry());
        film.setMark(new Functions().randFloatBetween((float)0.1,10));
        film.setDate(new Functions().randDateBetweenYear(1950,2000));
        film.setExpirationTime(new Functions().randIntBetween(10,20));
        film.setTrailer(new Functions().genRandHyperlink());
        film.setDuration(new Functions().randIntBetween(90,150));
        film.setPhotos(new Functions().genRandPhoto());
        if(film.getMark()>7.0){
            film.setPrice((float)6.99);
        }else{
            if(film.getMark()>=5.0){
                film.setPrice((float)4.99);
            }else{
                film.setPrice((float)2.99);
            }
        }
        film.setWatched(new Functions().genRandChartSeries());
        return  film;
    }

}
