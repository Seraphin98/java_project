package classes;

public class User {
    private String nickname;
    private String mail;
    private Abonament abonament;
    private String cardID;
    private int accountID;
    static private int nrOfUsers = 0;

    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setCardID(String cardID) {
        this.cardID = cardID;
    }

    public void setAccountID() {
        this.accountID = nrOfUsers++;
    }

    public void setAbonament(Abonament abonament) {
        this.abonament = abonament;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public User(){}

    @Override
    public String toString() {
        return "User{" +
                ", nickname='" + nickname + '\'' +
                ", mail='" + mail +
                ", abonament=" + abonament +
                ", cardID='" + cardID + '\'' +
                ", accountID=" + accountID +
                '}';
    }
}
