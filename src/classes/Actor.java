package classes;


import javafx.scene.image.Image;

import java.text.DecimalFormat;
import java.util.*;

public class Actor {
    private Image photo;
    private String name;
    private String surname;
    private GregorianCalendar birth;
    private float mark;

    public float getMark() {
        return mark;
    }
    public void setMark(float mark) {
        this.mark = mark;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public GregorianCalendar getBirth() {
        return birth;
    }
    public void setBirth(GregorianCalendar birth) {
        this.birth = birth;
    }
    public Image getPhoto() {
        return photo;
    }
    public void setPhoto(String location) {
        this.photo = new Image(location);
    }

    public Actor(){}

    public Actor genRandActor(){
        Actor actor = new Actor();
        GregorianCalendar date = new GregorianCalendar();
        actor.setName(new Functions().getRandName());
        actor.setSurname(new Functions().getRandSurname());
        date.set(Calendar.YEAR,new Functions().randIntBetween(1950,2000));
        date.set(Calendar.DAY_OF_YEAR,new Functions().randIntBetween(1,(date.getActualMaximum(Calendar.DAY_OF_YEAR))));
        actor.setBirth(date);
        actor.setMark(new Functions().randFloatBetween(0,10));
        actor.setPhoto(new Functions().genRandPhoto());
        return actor;
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("0.00");
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birth=" + new Functions().dateToString(birth) +
                ", mark=" + df.format(mark) +
                '}';
    }

}
