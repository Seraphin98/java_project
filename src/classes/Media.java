package classes;

import javafx.scene.chart.XYChart;
import javafx.scene.image.Image;

import java.util.GregorianCalendar;

public class Media{
    private String title;
    private String author;
    private String genre;
    private String country;
    private Image photos;
    private Distributor distributor;
    private GregorianCalendar date;
    private int duration;
    private float mark;
    private float price;
    private XYChart.Series watched;


    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public String getGenre() {
        return genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setCountries(String country) {
        this.country = country;
    }
    public Image getPhotos() {
        return photos;
    }
    public void setPhotos(String location) {
        this.photos = new Image(location);
    }
    public Distributor getDistributor() {
        return distributor;
    }
    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }
    public GregorianCalendar getDate() {
        return date;
    }
    public void setDate(GregorianCalendar date) {
        this.date = date;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }
    public float getMark() {
        return mark;
    }
    public void setMark(float mark) {
        this.mark = mark;
    }
    public float getPrice() {
        return price;
    }
    public void setPrice(float price) {
        this.price = price;
    }
    public XYChart.Series getWatched() {
        return watched;
    }
    public void setWatched(XYChart.Series watched) {
        this.watched = watched;
    }
}
