package classes;

import javafx.scene.chart.XYChart;

import java.time.LocalDate;
import java.util.*;

public class Functions {
    private ArrayList<String> titles1 = new ArrayList<>(Arrays.asList(
            "Smooth ","Lazy ","Badass ","Thrilling ","Hard ","Happy ","Next ","Dirty ","Fantastic ","Hyper ",
            "Weak ","Fairytale ","Fast ","Furious ","Mad ","The ","Classic ","Random "
    ));
    private ArrayList<String> titles2 = new ArrayList<>(Arrays.asList(
            "Criminal","Vacation","Excuse","Cop","Robot","Flirt","Dancing","Country","Example","Family",
            "Title","and Furious","Nonsense","Jack","Person","Badass","Kung-Fu Master","Mother"
    ));
    private ArrayList<String> genres = new ArrayList<>(Arrays.asList(
            "Drama","Comedy","Fantasy","Thriller","Horror","Sci-Fi","Kids","Family","Action","Documet"
    ));
    private ArrayList<String> countries = new ArrayList<>(Arrays.asList(
            "Poland","USA","GB","Sweden","Spain","Brazil","Canada","France","Russia"
    ));
    private ArrayList<String> names = new ArrayList<String>(Arrays.asList(
            "John","James","Arthur","Lucas","Aaron","Raymond","Jack",
            "Martha","Samantha","Angelina","Jane","Emily","Mary","Barbara"
    ));
    private ArrayList<String> surnames = new ArrayList<String>(Arrays.asList(
            "Black","Blunt","Smith","Kowalsky","Orange","Cash","Rose","Hopkins",
            "Wanda","White", "McKorey","Bond","Merc"
    ));
    private ArrayList<String> nicks = new ArrayList<>(Arrays.asList(
            "MovieWatcher","FilmManiac","MovieLord","SeriesManiac","StreamSlayer","SeriesAddict",
            "AllSeries1Night"
    ));
    private ArrayList<String> distributors = new ArrayList<>(Arrays.asList(
            "Best Distributor", "DroppingHotFilms", "Random Distributor", "ComeWatchIt", "GiraffeStudio",
            "sic!Studio", "RockBottomStories", "PUTSadStoriesStdio", "SadFrogPePe"
    ));
    private ArrayList<String> mails = new ArrayList<>(Arrays.asList(
            "@gmail.com","@yahoo.com","@outlook.com","@wp.pl","@mail.ru","@zoho.eu"
    ));
    private ArrayList<String> types = new ArrayList<>(Arrays.asList(
            "Basic","Family","Premium"
    ));
    private ArrayList<String> hyperLinks = new ArrayList<>(Arrays.asList(
            "https://www.youtube.com/watch?v=djV11Xbc914",
            "https://www.youtube.com/watch?v=FTQbiNvZqaY",
            "https://www.youtube.com/watch?v=kvDMlk3kSYg",
            "https://www.youtube.com/watch?v=god7hAPv8f0"
    ));
    private ArrayList<String> photos = new ArrayList<>(Arrays.asList(
            "/database/dudus.png",
            "/database/haddaway.png",
            "/database/jeff.png",
            "/database/mcgonagal.png",
            "/database/samuel.png",
            "/database/stoleit.png",
            "/database/toby.png",
            "/database/pepe.png"
    ));

    public String getRandTitle1() {
        return titles1.get(new Random().nextInt(titles1.size()));
    }
    public String getRandTitle2() {
        return titles2.get(new Random().nextInt(titles2.size()));
    }
    public String getRandGenre() {
        return genres.get(new Random().nextInt(genres.size()));
    }
    public String getRandCountry() {
        return countries.get(new Random().nextInt(countries.size()));
    }
    public String getRandName() {
        return names.get(new Random().nextInt(names.size()));
    }
    public String getRandSurname() {
        return surnames.get(new Random().nextInt(surnames.size()));
    }
    public String getRandNick() {
        return nicks.get(new Random().nextInt(nicks.size()));
    }
    public String getRandMail() {
        return mails.get(new Random().nextInt(mails.size()));
    }
    public String getRandAbonament(){
        return types.get(new Random().nextInt(types.size()));
    }
    public String genRandHyperlink(){
        return hyperLinks.get(new Random().nextInt(hyperLinks.size()));
    }
    public ArrayList<Distributor> getDistributorsPool(){
        ArrayList<Distributor> distributorsPool = new ArrayList<>();
        for(String string : distributors){
            Distributor distributor = new Distributor().genRandDistributor(string);
            distributorsPool.add(distributor);
        }
        return distributorsPool;
    }
    public Distributor getDistributorFromPool(ArrayList<Distributor> distributors){
        return distributors.get(new Random().nextInt(distributors.size()));
    }
    public ArrayList<Abonament> getAbonamentsPool(){
        ArrayList<Abonament> abonaments = new ArrayList<>();
        for(String string : types){
            abonaments.add(new Abonament(string));
        }
        return abonaments;
    }
    public String genRandPhoto (){
        return photos.get(new Random().nextInt(photos.size()));
    }
    public String randCardID(){
        String cardID = "";
        for(int i =0;i<9;i++){
            cardID += new Integer(new Random().nextInt(10)).toString();
        }
        return cardID;
    }
    public String dateToString(GregorianCalendar date){
        return date.get(Calendar.DAY_OF_MONTH) + "." + (date.get(Calendar.MONTH)+1) + "." + date.get(Calendar.YEAR);
    }
    public int randIntBetween(int min, int max){
        if(min>=max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        return new Random().nextInt((max - min) + 1) + min;
    }
    public float randFloatBetween(float min, float max){
        if(min>=max){
            throw new IllegalArgumentException("min must be greater than max");
        }
        return (new Random().nextFloat() * (max - min) + min);
    }
    public GregorianCalendar randDateBetweenYear(int min,int max){
        GregorianCalendar date = new GregorianCalendar();
        date.set(Calendar.YEAR,randIntBetween(min,max));
        date.set(Calendar.DAY_OF_YEAR,randIntBetween(1,date.getActualMaximum(Calendar.DAY_OF_YEAR)));
        return date;
    }
    public ArrayList<Actor> getActorsFromPool(ArrayList<Actor> pool, int nr){
        ArrayList<Actor> actors = new ArrayList<>();
        Actor tmpActor;
        if(pool.size()<nr) {
            throw new IllegalArgumentException("must be greater than " + nr);
        }
        while(actors.size()<nr){
            tmpActor = pool.get(new Random().nextInt(pool.size()));
            if(!actors.contains(tmpActor)){
                actors.add(tmpActor);
            }
        }
        return actors;
    }
    public String genDescription (String type){
        String text =  "This is an awesome " + type + " here is the plot :\n\t";
        for(int i =0;i<randIntBetween(10,40);i++){
            text += " blah";
        }
        text += ".";
        return text;
    }
    public XYChart.Series genRandChartSeries(){
        XYChart.Series series = new XYChart.Series();
        int years = LocalDate.now().getYear() - 15;
        for(int i =0;i<15;i++){
            series.getData().add(new XYChart.Data(years + i,new Functions().randIntBetween(10,50)));
        }
        return series;
    }
}
