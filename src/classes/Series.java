package classes;

import java.util.ArrayList;

public class Series extends Media{
    private ArrayList<Actor> actors;
    private int nrOfSeasons;
    private ArrayList<Season> seasons;
    private int nrOfActors;

    public ArrayList<Actor> getActors() {
        return actors;
    }
    public void setActors(ArrayList<Actor> actors) {
        this.actors = actors;
    }
    public ArrayList<Season> getSeasons() {
        return seasons;
    }
    public void setSeasons(ArrayList<Season> seasons) {
        this.seasons = seasons;
    }
    public int getNrOfSeasons() {
        return nrOfSeasons;
    }
    public void setNrOfSeasons(int nrOfSeasons) {
        this.nrOfSeasons = nrOfSeasons;
    }
    public int getNrOfActors() {
        return nrOfActors;
    }
    public void setNrOfActors(int nrOfActors) {
        this.nrOfActors = nrOfActors;
    }

    public Series(){};
    public Series genRandSeries(){
        Series series = new Series();
        series.setTitle(new Functions().getRandTitle1() + new Functions().getRandTitle2());
        series.setAuthor(new Functions().getRandName() + " " + new Functions().getRandSurname());
        series.setGenre(new Functions().getRandGenre());
        series.setCountries(new Functions().getRandCountry());
        series.setNrOfSeasons(new Functions().randIntBetween(1,5));
        ArrayList<Season> tmpSeasons = new ArrayList<>();
        for(int i = 0;i<series.getNrOfSeasons();i++) {
            Season season = new Season().genRandSeason();
            season.setTitle("Season " + (i+1));
            tmpSeasons.add(season);
            series.setDuration(series.getDuration() + season.getDuration());
            series.setPrice(series.getPrice() + season.getPrice());
        }
        series.setPrice((float)(series.getPrice() * 0.8));
        series.setSeasons(tmpSeasons);
        series.setDate(series.getSeasons().get(0).getDateOfPremiere());
        series.setNrOfActors(new Functions().randIntBetween(5,10));
        series.setMark(new Functions().randFloatBetween((float)0.1,10));
        series.setPhotos(new Functions().genRandPhoto());
        series.setWatched(new Functions().genRandChartSeries());
        return series;
    }
    @Override
    public String toString() {
        return "Series : " + getTitle() + "{" +
                "author : " + getAuthor() +
                ", genre : " + getGenre() +
                ", distributor : " + getDistributor() +
                ", date : " + new Functions().dateToString(getDate()) +
                ", duration : " + getDuration() +
                ", mark : " + getMark() +
                ", actors=" + actors +
                ", seasons=" + seasons +
                '}';
    }
}
