package classes;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;

public class Discount {
    private GregorianCalendar begin;
    private GregorianCalendar end;
    private int percentage;

    public Discount(){}


    public GregorianCalendar getBegin() {
        return begin;
    }
    public void setBegin(GregorianCalendar begin) {
        this.begin = begin;
    }
    public GregorianCalendar getEnd() {
        return end;
    }
    public void setEnd(GregorianCalendar end) {
        this.end = end;
    }
    public int getPercentage() {
        return percentage;
    }
    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public Discount genRandDiscount(){
        Discount discount = new Discount();
        int percentage = new Random().nextInt(60) + 1;
        GregorianCalendar endDate = new GregorianCalendar();
        GregorianCalendar today = new GregorianCalendar();
        today.set(Calendar.YEAR,LocalDate.now().getYear());
        today.set(Calendar.DAY_OF_YEAR,LocalDate.now().getDayOfYear());
        discount.setPercentage(percentage);
        today.add(Calendar.DAY_OF_YEAR,new Random().nextInt(21) + 10);
        endDate.set(Calendar.YEAR,LocalDate.now().getYear());
        endDate.set(Calendar.DAY_OF_YEAR,LocalDate.now().getDayOfYear());
        discount.setBegin(today);
        if(percentage>50){
            endDate.add(Calendar.DAY_OF_YEAR,new Random().nextInt(21)+ 12);
        }else {
            if (percentage > 30) {
                endDate.add(Calendar.DAY_OF_YEAR, new Random().nextInt(35) + 28);
            } else {
                endDate.add(Calendar.DAY_OF_YEAR, new Random().nextInt(35) + 35);
            }
        }
        discount.setEnd(endDate);
        return discount;
    }

    @Override
    public String toString() {
        return "Discount{" +
                "begin=" + new Functions().dateToString(begin) +
                ", end=" + new Functions().dateToString(end) +
                ", percentage=" + percentage +
                '}';
    }
}
