package classes;


import java.util.GregorianCalendar;

public class Episode {
    private String title;
    private int length;
    private float mark;
    private GregorianCalendar epPremiere;

    public Episode(){}

    public String getTitle() {
        return title;
    }
    public void setEpisodeTitle(String title) {
        this.title = title;
    }
    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public void setEpPremiere(GregorianCalendar epPremiere) {
        this.epPremiere = epPremiere;
    }

    @Override
    public String toString() {
        return "Episode{" +
                "title='" + title + '\'' +
                ", length=" + length +
                ", mark=" + mark +
                ", epPremiere=" + new Functions().dateToString(epPremiere) +
                '}';
    }
}
