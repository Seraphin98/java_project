package classes;


public class LiveStream extends Media {
    private Discount discount;


    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public LiveStream genRandStream (){
        LiveStream liveStream = new LiveStream();
        liveStream.setDate(new Functions().randDateBetweenYear(2019,2020));
        liveStream.setTitle(new Functions().getRandTitle1() + new Functions().getRandTitle2());
        liveStream.setAuthor(new Functions().getRandName() + " " + new Functions().getRandSurname());
        liveStream.setCountries(new Functions().getRandCountry());
        liveStream.setDuration(new Functions().randIntBetween(120,250));
        liveStream.setGenre(new Functions().getRandGenre());
        liveStream.setPhotos(new Functions().genRandPhoto());
        liveStream.setMark(new Functions().randFloatBetween((float)0.1,10));
        if(liveStream.getMark()>7.0){
            liveStream.setPrice((float)7.99);
        }else{
            if(liveStream.getMark()>=5.0){
                liveStream.setPrice((float)5.99);
            }else{
                liveStream.setPrice((float)3.99);
            }
        }
        return liveStream;
    }
}
