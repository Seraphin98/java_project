package sample;

import classes.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;

public class MediaAddingController implements Initializable {
    @FXML
    private Label numberLabel;
    @FXML
    private TextField titleText;
    @FXML
    private TextField authorText;
    @FXML
    private TextField genreText;
    @FXML
    private TextField countryText;
    @FXML
    private TextField durationText;
    @FXML
    private TextField dayText;
    @FXML
    private TextField priceText;
    @FXML
    private TextField markText;
    @FXML
    private TextField nrText;
    @FXML
    private TextField yearText;
    @FXML
    private TextField monthText;
    @FXML
    private ComboBox distributorBox;
    @FXML
    private Label seasonLabel;
    @FXML
    private TextField actorsText;
    @FXML
    private Label durationLabel;
    private String type;
    private ArrayList<Distributor> distributors;
    private ArrayList<Actor> actorsPool;
    private ArrayList<Actor> actorsForMedia(ArrayList<Actor> actorsPool,int nr){
        ArrayList<Actor> tmpPool = new ArrayList<>();
        while(tmpPool.size()<nr){
            Actor actor = new Actor().genRandActor();
            if(!actorsPool.contains(actor)){
                actorsPool.add(actor);
                tmpPool.add(actor);
            }
        }
        return tmpPool;
    }
    public void setType(String type) {
        this.type = type;
        switch(type){
            case "Series":{
                durationText.setVisible(false);
                durationLabel.setVisible(false);
                break;
            }
            case "Film":{
                seasonLabel.textProperty().set("Expiration time (days)");
                break;
            }
            case "Live Stream":{
                actorsText.setVisible(false);
                seasonLabel.setVisible(false);
                numberLabel.setVisible(false);
                nrText.setVisible(false);
                break;
            }
            default:{
                throw new IllegalArgumentException("Wrong Arguments!");
            }
        }
    }
    public void setDistributors(ArrayList<Distributor> distributors) {
        ObservableList<String> distributorsList = FXCollections.observableArrayList();
        this.distributors = distributors;
        for(Distributor distributor : distributors){
            distributorsList.add(distributor.getNickname());
        }
        distributorBox.setItems(distributorsList);

    }
    public void setActorsPool(ArrayList<Actor> actorsPool){
        this.actorsPool = actorsPool;
    }

    @FXML
    public void submitClicked(ActionEvent actionEvent) {
        Controller controller = Controller.getRunningController();
        GregorianCalendar date = new GregorianCalendar();
        date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayText.getText()));
        date.set(Calendar.MONTH,Integer.parseInt(monthText.getText()) - 1);
        date.set(Calendar.YEAR,Integer.parseInt(yearText.getText()));
        switch (type){
            case "Series": {
                durationText.setVisible(false);
                Series series = new Series();
                ArrayList<Season> seasons = new ArrayList<>();
                series.setTitle(titleText.getText());
                series.setAuthor(titleText.getText());
                series.setGenre(genreText.getText());
                series.setNrOfSeasons(Integer.parseInt(nrText.getText()));
                series.setDate(date);
                for (int i = 0; i < series.getNrOfSeasons(); i++) {
                    Season season = new Season().genRandSeason();
                    season.setTitle("Season " + (i + 1));
                    seasons.add(season);
                    series.setPrice(series.getPrice() + season.getPrice());
                    series.setDuration(series.getDuration() + season.getDuration());
                }
                series.setCountries(countryText.getText());
                series.setPrice(Float.parseFloat(priceText.getText()));
                for (Distributor distributor : distributors) {
                    if (distributor.getNickname() == distributorBox.getValue()) {
                        series.setDistributor(distributor);
                        break;
                    }
                }
                series.setSeasons(seasons);
                series.setMark(Float.parseFloat(markText.getText()));
                series.setNrOfActors(Integer.parseInt(actorsText.getText()));
                series.setActors(actorsForMedia(actorsPool,series.getNrOfActors()));
                series.setPhotos(new Functions().genRandPhoto());
                series.setWatched(new Functions().genRandChartSeries());
                controller.addSeries(series);
                break;
            }
            case "Film": {
                Film film = new Film();
                film.setTitle(titleText.getText());
                film.setAuthor(authorText.getText());
                film.setGenre(genreText.getText());
                film.setDuration(Integer.parseInt(durationText.getText()));
                film.setPrice(Float.parseFloat(priceText.getText()));
                film.setMark(Float.parseFloat(markText.getText()));
                film.setCountries(countryText.getText());
                film.setDate(date);
                for (Distributor distributor : distributors) {
                    if (distributor.getNickname() == distributorBox.getValue()) {
                        film.setDistributor(distributor);
                    }
                }
                film.setNrOfActors(Integer.parseInt(actorsText.getText()));
                film.setActors(actorsForMedia(actorsPool,film.getNrOfActors()));
                film.setExpirationTime(Integer.parseInt(nrText.getText()));
                film.setPhotos(new Functions().genRandPhoto());
                film.setWatched(new Functions().genRandChartSeries());
                controller.addFilm(film);
                break;
            }
            case "Live Stream": {
                LiveStream liveStream = new LiveStream();
                liveStream.setTitle(titleText.getText());
                liveStream.setAuthor(authorText.getText());
                liveStream.setGenre(genreText.getText());
                liveStream.setCountries(countryText.getText());
                liveStream.setPrice(Float.parseFloat(priceText.getText()));
                liveStream.setMark(Float.parseFloat(markText.getText()));
                liveStream.setDate(date);
                liveStream.setDuration(Integer.parseInt(durationText.getText()));
                for (Distributor distributor : distributors) {
                    if (distributor.getNickname() == distributorBox.getValue()) {
                        liveStream.setDistributor(distributor);
                    }
                }
                liveStream.setPhotos(new Functions().genRandPhoto());
                controller.addStream(liveStream);
                break;
            }
            default:{
                throw new IllegalArgumentException("wrong arguments");
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
