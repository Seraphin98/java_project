package sample;

import classes.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    static private Controller runningController;

    private final ObservableList<String> actors = FXCollections.observableArrayList();
    private Discount discount;
    private TreeItem<String> root, seriesBranch, filmsBranch, streamsBranch;
    private ArrayList<Series> seriesPool = new ArrayList<>();
    private ArrayList<Film> filmsPool = new ArrayList<>();
    private ArrayList<LiveStream> streamsPool = new ArrayList<>();
    private ArrayList<Actor> actorsPool = new ArrayList<>();
    private final ArrayList<User> usersPool = new ArrayList<>();
    private ArrayList<Distributor> distributorsPool = new Functions().getDistributorsPool();
    private final ArrayList<Abonament> abonamentsPool = new Functions().getAbonamentsPool();
    private static final DecimalFormat df = new DecimalFormat("#.##");
    private final NumberAxis xAxis = new NumberAxis();
    private final NumberAxis yAxis = new NumberAxis();

    public static Controller getRunningController() {
        return runningController;
    }
    public static void setRunningController(Controller runningController) {
        Controller.runningController = runningController;
    }
    private void setDiscount(Media media){
        float percentage = (float)(100 - discount.getPercentage());
        float newPrice = media.getPrice() * percentage /100;
        String text = "New Price : " + df.format(newPrice) + "$" +
                "\nStarts : " + new Functions().dateToString(discount.getBegin()) +
                "\nEnds : " + new Functions().dateToString(discount.getEnd());
        discountLabel.textProperty().set(text);
    }
    private void setChart(Media media){
        chart.getData().clear();
        chart.getData().removeAll();
        xAxis.setLowerBound(1950);
        chart.setTitle("Watched during Years");
        chart.getData().add(media.getWatched());
    }
    public void addFilm(Film film){
        Film addedfilm = film;
        filmsPool.add(addedfilm);
        makeBranch(addedfilm.getTitle(),filmsBranch);
        mediaList.refresh();
    }
    public void addSeries(Series series) {
        Series addedSeries = series;
        seriesPool.add(addedSeries);
        makeBranch(addedSeries.getTitle(),seriesBranch);
        mediaList.refresh();
    }
    public void addStream(LiveStream liveStream){
        LiveStream addedStream = liveStream;
        streamsPool.add(addedStream);
        makeBranch(addedStream.getTitle(),streamsBranch);
        mediaList.refresh();
    }
    public void addUser(User user){
        User addedUser = user;
        usersPool.add(addedUser);
        System.out.println("added user : " + user);
    }
    public void addDistributor(Distributor distributor){
        Distributor addedDistributor = distributor;
        distributorsPool.add(addedDistributor);
        System.out.println("added distributor : " + addedDistributor);
    }
    private void peopleCliced(String type){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/peopleAdding.fxml"));
        try {
            Parent root = loader.load();
            PeopleAddingController peopleAddingController = loader.getController();
            peopleAddingController.setAbonaments(abonamentsPool);
            Stage stage = new Stage();
            stage.setTitle("Adding " + type);
            switch (type) {
                case "User": {
                    peopleAddingController.setType(type);
                    break;
                }
                case "Distributor": {
                    peopleAddingController.setType(type);
                    break;
                }
                default: {
                    throw new IllegalArgumentException("Wrong Arguments!");
                }
            }
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void mediaClicked(String type){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/mediaAdding.fxml"));
            Parent root = loader.load();
            MediaAddingController addingController = loader.getController();
            addingController.setDistributors(distributorsPool);
            addingController.setActorsPool(actorsPool);
            Stage stage = new Stage();
            stage.setTitle("Adding " + type);
            switch (type){
                case "Series":{
                    addingController.setType(type);
                    break;
                }
                case "Film":{
                    addingController.setType(type);
                    break;
                }
                case "Live Stream":{
                    addingController.setType("Live Stream");
                    break;
                }
                default:{
                    throw new IllegalArgumentException("Wrong Arguments!");
                }
            }
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void displayActorInfo(Actor actor){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/actorInfo.fxml"));
        try {
            Parent root = loader.load();
            ActorInfoController actorInfoController = loader.getController();
            actorInfoController.setActor(actor);
            Stage stage = new Stage();
            stage.setTitle(actor.getName() + " " + actor.getSurname() + " Info");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void iniTree(){
        root = new TreeItem<>();
        root.setExpanded(true);
        mediaList.setShowRoot(false);
        mediaList.setRoot(root);
        seriesBranch = makeBranch("Series",root);
        filmsBranch = makeBranch("Films",root);
        streamsBranch = makeBranch("Live Streams",root);
    }
    private TreeItem<String> makeBranch(String title, TreeItem<String> root){
        TreeItem<String> item = new TreeItem<>(title);
        item.setExpanded(true);
        root.getChildren().add(item);
        return item;
    }

    @FXML
    private TextField searchField;
    @FXML
    private Label markLabel;
    @FXML
    private Label titleLabel;
    @FXML
    private Label authorLabel;
    @FXML
    private Label genreLabel;
    @FXML
    private Label distributor;
    @FXML
    private Label dateLabel;
    @FXML
    private Label durationLabel;
    @FXML
    private ListView actorList;
    @FXML
    private Hyperlink trailer;
    @FXML
    private Label priceLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private TreeView mediaList;
    @FXML
    private ComboBox seasonList;
    @FXML
    private ComboBox episodeList;
    @FXML
    private ImageView image;
    @FXML
    private Label discountLabel;
    @FXML
    private Label percentageLabel;
    @FXML
    private LineChart<Number,Number> chart = new LineChart<Number, Number>(xAxis,yAxis);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        chart.animatedProperty().setValue(false);
        discount = new Discount().genRandDiscount();
        iniTree();
        for(int i = 0;i<50;i++) {
            Actor addActor = new Actor().genRandActor();
            for(Actor actor : actorsPool){
                while ((addActor.getName()+addActor.getSurname()).equals(actor.getName()+actor.getSurname())){
                    addActor = new Actor().genRandActor();
                }
            }
            actorsPool.add(addActor);
        }
        ArrayList<Actor> filmActors = new ArrayList<>();
        ArrayList<Actor> seriesActors = new ArrayList<>();
        for(int i =0;i<10;i++){
            filmActors.clear();
            seriesActors.clear();
            seriesPool.add(i,new Series().genRandSeries());
            seriesPool.get(i).setActors(new Functions().getActorsFromPool(actorsPool,10));
            seriesPool.get(i).setDistributor(new Functions().getDistributorFromPool(distributorsPool));
            makeBranch(seriesPool.get(i).getTitle(),seriesBranch);
            filmsPool.add(i,new Film().genRandFilms());
            filmsPool.get(i).setActors(new Functions().getActorsFromPool(actorsPool,10));
            filmsPool.get(i).setDistributor(new Functions().getDistributorFromPool(distributorsPool));
            makeBranch(filmsPool.get(i).getTitle(),filmsBranch);
            streamsPool.add(i,new LiveStream().genRandStream());
            streamsPool.get(i).setDistributor(new Functions().getDistributorFromPool(distributorsPool));
            makeBranch(streamsPool.get(i).getTitle(),streamsBranch);
        }
        mediaList.setEditable(false);
        titleLabel.textProperty().set("");
        authorLabel.textProperty().set("");
        genreLabel.textProperty().set("");
        distributor.textProperty().set("");
        dateLabel.textProperty().set("");
        durationLabel.textProperty().set("");
        descriptionLabel.textProperty().set("");
        priceLabel.textProperty().set("");
        mediaList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

        @FXML
    private void selectedMedia(MouseEvent mouseEvent) {
        discountLabel.setVisible(true);
        percentageLabel.setVisible(true);
        trailer.setVisible(false);
        seasonList.setVisible(false);
        episodeList.setVisible(false);
        actorList.setVisible(true);
        chart.setVisible(true);
        TreeItem<String> selectedMedia = (TreeItem<String>) mediaList.getSelectionModel().getSelectedItem();
        if(selectedMedia.getParent().getValue() == "Series"){
            Series selectedSeries = new Series();
            for (Series series: seriesPool){
                if(series.getTitle() == selectedMedia.getValue()){
                    selectedSeries = series;
                    break;
                }
            }
            ObservableList<String> seasons = FXCollections.observableArrayList();
            descriptionLabel.textProperty().set("");
            seasonList.getSelectionModel().clearSelection();
            seasonList.setVisible(true);
            episodeList.getSelectionModel().clearSelection();
            episodeList.setVisible(true);
            percentageLabel.setVisible(false);
            discountLabel.setVisible(false);
            titleLabel.textProperty().set(selectedSeries.getTitle());
            authorLabel.textProperty().set(selectedSeries.getAuthor());
            genreLabel.textProperty().set(selectedSeries.getGenre());
            distributor.textProperty().set(selectedSeries.getDistributor().getNickname());
            dateLabel.textProperty().set(new Functions().dateToString(selectedSeries.getDate()));
            durationLabel.textProperty().set(selectedSeries.getDuration() + " min");
            markLabel.textProperty().set(df.format(selectedSeries.getMark()) + "/10");
            priceLabel.textProperty().set(df.format(selectedSeries.getPrice()) + " $");
            percentageLabel.textProperty().set("Discount : " + discount.getPercentage() + "%");
            setChart(selectedSeries);
            image.setImage(selectedSeries.getPhotos());
            actors.clear();
            for (Actor actor  :selectedSeries.getActors()) {
                actors.add(actor.getName() + " " + actor.getSurname());
            }
            actorList.setItems(actors);
            for(Season season : selectedSeries.getSeasons()){
                seasons.add(season.getTitle());
            }
            seasonList.setItems(seasons);
        }
        if(selectedMedia.getParent().getValue() == "Films"){
            Film selectedFilm = new Film();
            for(Film film : filmsPool){
                if(film.getTitle() == selectedMedia.getValue()){
                    selectedFilm = film;
                    break;
                }
            }
            trailer.setVisible(true);
            trailer.setVisited(false);
            titleLabel.textProperty().set(selectedFilm.getTitle());
            authorLabel.textProperty().set(selectedFilm.getAuthor());
            genreLabel.textProperty().set(selectedFilm.getGenre());
            distributor.textProperty().set(selectedFilm.getDistributor().getNickname());
            dateLabel.textProperty().set(new Functions().dateToString(selectedFilm.getDate()));
            durationLabel.textProperty().set(selectedFilm.getDuration() + " min");
            markLabel.textProperty().set(df.format(selectedFilm.getMark()) + "/10");
            priceLabel.textProperty().set(df.format(selectedFilm.getPrice()) + " $");
            descriptionLabel.textProperty().set(new Functions().genDescription("Film"));
            percentageLabel.textProperty().set("Discountt : " + discount.getPercentage() + "%");
            selectedFilm.setDiscount(discount);
            setDiscount(selectedFilm);
            setChart(selectedFilm);
            image.setImage(selectedFilm.getPhotos());
            actors.clear();
            for(Actor actor : selectedFilm.getActors()){
                actors.add(actor.getName() + " " + actor.getSurname());
            }
            actorList.setItems(actors);
        }
        if(selectedMedia.getParent().getValue() == "Live Streams"){
            LiveStream selectedLiveStream = new LiveStream();
            for(LiveStream liveStream : streamsPool){
                if(liveStream.getTitle() == selectedMedia.getValue()){
                    selectedLiveStream = liveStream;
                    break;
                }
            }
            chart.getData().clear();
            chart.getData().removeAll();
            trailer.setVisible(false);
            actorList.setVisible(false);
            chart.setVisible(false);
            titleLabel.textProperty().set(selectedLiveStream.getTitle());
            authorLabel.textProperty().set(selectedLiveStream.getAuthor());
            genreLabel.textProperty().set(selectedLiveStream.getGenre());
            distributor.textProperty().set(selectedLiveStream.getDistributor().getNickname());
            dateLabel.textProperty().set(new Functions().dateToString(selectedLiveStream.getDate()));
            durationLabel.textProperty().set(selectedLiveStream.getDuration() + " min");
            markLabel.textProperty().set(df.format(selectedLiveStream.getMark()) + "/10");
            priceLabel.textProperty().set(df.format(selectedLiveStream.getPrice()) + "$");
            descriptionLabel.textProperty().set(new Functions().genDescription("Live Stream"));
            percentageLabel.textProperty().set("Discountt : " + discount.getPercentage() + "%");
            setDiscount(selectedLiveStream);
            selectedLiveStream.setDiscount(discount);
            image.setImage(selectedLiveStream.getPhotos());
        }
    }

    @FXML
    private void seasonClicked() {
        TreeItem<String> selectedMedia = (TreeItem<String>) mediaList.getSelectionModel().getSelectedItem();
        ObservableList<String> episodes = FXCollections.observableArrayList();
        for(Series series : seriesPool){
            if(series.getTitle() == selectedMedia.getValue()){
                for(Season season : series.getSeasons()){
                    if(season.getTitle() == seasonList.getValue()){
                        for(Episode episode : season.getEpisodes()){
                            episodes.add(episode.getTitle());
                        }
                        break;
                    }
                }
                break;
            }
        }
        episodeList.setItems(episodes);
    }

    @FXML
    private void episodeClicked() {
        TreeItem<String> selectedMedia = (TreeItem<String>) mediaList.getSelectionModel().getSelectedItem();
        String selectedSeason = seasonList.getValue().toString();
        for(Series series : seriesPool){
            if(series.getTitle() == selectedMedia.getValue()){
                for(Season season : series.getSeasons()){
                    if(season.getTitle() == selectedSeason){
                        for(Episode episode: season.getEpisodes()){
                            if(episode.getTitle() == episodeList.getValue()){
                                descriptionLabel.setWrapText(true);
                                descriptionLabel.textProperty().set(new Functions().genDescription("Episode"));
                                break;
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
    }

    @FXML
    private void trailerClicked() {
        TreeItem<String> sfilm = (TreeItem<String>)mediaList.getSelectionModel().getSelectedItem();
        for(Film film : filmsPool){
            if(film.getTitle() == sfilm.getValue()){
                Desktop desktop = Desktop.getDesktop();
                try {
                    desktop.browse(new URI(film.getTrailer()));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @FXML
    private void filmClicked(ActionEvent actionEvent) {
        mediaClicked("Film");
    }

    @FXML
    private void seriesClicked(ActionEvent actionEvent) {
        mediaClicked("Series");
    }

    @FXML
    private void streamClicked(ActionEvent actionEvent) {
        mediaClicked("Live Stream");
    }

    @FXML
    private void distributorClicked(ActionEvent actionEvent) {
        peopleCliced("Distributor");
    }

    @FXML
    private void userClicked(ActionEvent actionEvent) {
        peopleCliced("User");
    }

    @FXML
    public void actorInfo(MouseEvent mouseEvent) {
        String selectedActor = actorList.getSelectionModel().getSelectedItem().toString();
        for(Actor actor : actorsPool){
            String actorName = actor.getName() + " " + actor.getSurname();
            if(actorName.equals(selectedActor)) {
                displayActorInfo(actor);
                break;
            }
        }
    }

    @FXML
    private void searchClicked(ActionEvent actionEvent) {
        String search = searchField.getText();
        mediaList.setRoot(null);
        iniTree();
        for(Series series : seriesPool){
            if(series.getTitle().contains(search)){
                makeBranch(series.getTitle(),seriesBranch);
            }
        }
        for(Film film : filmsPool){
            if(film.getTitle().contains(search)){
                makeBranch(film.getTitle(),filmsBranch);
            }
        }
        for(LiveStream liveStream : streamsPool){
            if(liveStream.getTitle().contains(search)){
                makeBranch(liveStream.getTitle(),streamsBranch);
            }
        }
    }

    @FXML
    private void refreshTree() {
        searchField.textProperty().set("");
        mediaList.setRoot(null);
        iniTree();
        for(Series series : seriesPool){
            makeBranch(series.getTitle(),seriesBranch);
        }
        for(Film film : filmsPool){
            makeBranch(film.getTitle(),filmsBranch);
        }
        for(LiveStream liveStream : streamsPool){
            makeBranch(liveStream.getTitle(),streamsBranch);
        }
    }

    @FXML
    private void deleteClicked(ActionEvent actionEvent) {
        TreeItem<String> selectedMedia = (TreeItem<String>) mediaList.getSelectionModel().getSelectedItem();
        if(selectedMedia.getParent().getValue() == "Series"){
            for(Series series : seriesPool) {
                if (series.getTitle().equals(selectedMedia.getValue())) {
                    seriesPool.remove(series);
                    break;
                }
            }
        }
        if(selectedMedia.getParent().getValue() == "Films"){
            for(Film film : filmsPool){
                if(film.getTitle().equals(selectedMedia.getValue())){
                    filmsPool.remove(film);
                    break;
                }
            }
        }
        if(selectedMedia.getValue() == "Live Streams"){
            for(LiveStream liveStream : streamsPool){
                if(liveStream.getTitle().equals(selectedMedia.getValue())){
                    streamsPool.remove(liveStream);
                    break;
                }
            }
        }
        refreshTree();
    }
}
