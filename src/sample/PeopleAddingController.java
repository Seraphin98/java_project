package sample;

import classes.Abonament;
import classes.Distributor;
import classes.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class PeopleAddingController implements Initializable {
    @FXML
    private TextField nickText;
    @FXML
    private TextField mailText;
    @FXML
    private TextField cardText;
    @FXML
    private ComboBox abonamentBox;
    @FXML
    private Label errorLabel;
    @FXML
    private Label abonamentLabel;
    @FXML
    private Label mailLabel;

    private String type;
    private ArrayList<Abonament> abonaments;

    public void setType(String type) {
        this.type = type;
        if(type.equals("Distributor")){
            abonamentBox.setVisible(false);
            abonamentLabel.setVisible(false);
            mailLabel.textProperty().set("Money in da bank");
        }
    }
    public void setAbonaments(ArrayList<Abonament> abonaments) {
        ObservableList<String> abonamentsPool = FXCollections.observableArrayList();
        this.abonaments = abonaments;
        for(Abonament abonament : abonaments){
            abonamentsPool.add(abonament.getType());
        }
        abonamentBox.setItems(abonamentsPool);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void submitClicked(ActionEvent actionEvent) {
        Controller controller = Controller.getRunningController();
        switch(type){
            case "User": {
                User user = new User();
                user.setNickname(nickText.getText());
                user.setMail(mailText.getText());
                user.setCardID(cardText.getText());
                for (Abonament abonament : abonaments) {
                    if (abonament.getType() == abonamentBox.getValue()) {
                        user.setAbonament(abonament);
                        break;
                    }
                }
                user.setAccountID();
                controller.addUser(user);
                break;
            }
            case "Distributor": {
                Distributor distributor = new Distributor(nickText.getText(), cardText.getText(), Float.parseFloat(mailText.getText()));
                controller.addDistributor(distributor);
                break;
            }
            default:{
                throw new IllegalArgumentException("wrong arguments");
            }
        }
    }
}
