package sample;

import classes.Actor;
import classes.Functions;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class ActorInfoController implements Initializable {

    @FXML
    private Label markLabel;
    @FXML
    private Label dateLabel;
    @FXML
    private Label surnameLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private ImageView imageView;

    private static DecimalFormat df = new DecimalFormat("##.##");

    public void setActor (Actor actor){
        nameLabel.textProperty().set(actor.getName());
        surnameLabel.textProperty().set(actor.getSurname());
        dateLabel.textProperty().set(new Functions().dateToString(actor.getBirth()));
        markLabel.textProperty().set(df.format(actor.getMark()) + "/10");
        imageView.setImage(actor.getPhoto());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) { }
}
